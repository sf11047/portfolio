const romeo = 
`I love you—
the way feathers float through clouds, listless
and soft—no, I mean arrows, pointed and loaded,
but not like Eros, Eros is boring, like—
drones, my millennial lover, this is the future,
you, my target, but not targeted—I love you most violently,
like the music on the radio, Apollo’s song heralding the sun—
like sun! I love you like the sun:
you, the center of my dark orbit, you, my very soul,
the smallest spinning atom in my heart, you,
my heart, cloven from my own chest, the cavity
of my ribs, my angel, my—

I love the twist of your hair
curled too tight around your finger
when you get embarrassed.
`;

const suits = 
`I am your boy drowning in this country, who doesn’t know the word
drowning, drowning in thread, threading needles that will stich
Versace dresses onto the bodies of women who have never touched
a stove before. I am your boy choking in this country of credit, accredited
thief for laboring where pale hands would never want to. I am your boy
in this country, who learned your bleached language with all its rules,
ruled alien in turn for my effort. I am your boy in this country of promises,
promised concrete for a bed no better than the dirt back home,
where city smog hasn’t flooded the skies purple-red. I am your boy
on stolen land, landed head-first, feet tumbling into the sewage
of a dream stolen. I am your boy.
`;

const tactical = 
`Sun
glares, cutting
through glass, the off-white tint carving sharp
lines through blinds we forgot
to close, too interested in

each
other, in
mutual destruction. That phantom grip—
your hand too tight around
my wrist, unkind, designed to wound—

left
your message.
Carrots grow sweeter in the winter,
bred solely for sucrose.
Your hair fans around your sleeping

face,
the quiet
of your profile tempting my feet
in a frame of stillness
while leaving, my weakness exposed.
`

const hold = 
`说中文. 你是中国人
Don’t speak English. You’re Chinese.
Words thrown casually into
dinner time conversation target
me, the punchline to a joke only
half-joking, my heart heavy
on this night of cricket-song.
It is my blood that’s yours,
it is the rest that’s not.
Outside the next day
through the fog-hazed air
climbing higher and higher,
I stood at the foot of history,
a temple centuries old,
and around me, as one,
my family kneeled.
Mother, Father, Auntie, Uncle
A dynasty of faith
lost to my own ignorance.
Half a generation,
and it’s gone.

But let the weathered stone crumble
and the birds take their perch.
Mama, am I still your daughter? Hold me,
remind me who I am.
`

const aubade = 
`Aurora lifts one trailing hand,
the movement warning Polaris,
her fingers sparking daybreak,
the tide of twenty-four new hours
crashing over me once again.
The yelling starts. It is no one
voice in particular—the clothes
a jumbled mess, last night’s email
(we are sorry to inform you),
the teetering, unopened stack
of bills, my mother’s glaring scorn,
my hands shaking in the morning
pouring coffee like gasoline,
the smell of bacon sizzling, made,
I know, with love, nonexistent
time taken from your schedule
just for me. It’s too loud.
`
