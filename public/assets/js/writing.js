/**
 * Notebook DOM element.
 * Use Query Selector with css class selector `.` to get element.
 */
const notebook = document.querySelector(".notebook");

/**
 * Creates a new DOM Element of the given tag
 *
 * @param {string} [tag=div] - HTMLElement tag
 * @returns {HTMLElement}
 */
const elem = (tag = "div") => document.createElement(tag);

/**
 * Models of Writing page list items
 *
 * @type {{type: string, titles: {title: string, pdfURI: string, text: string}[]}[]}
 */
const model = [
  {
    type: "Academic Writing",
    titles: [
      {
        title: "Smart Cities: A Survey",
        pdfURI: "assets/pdfs/Smart_City.pdf",
        text: smartCities,
      },
      {
        title: "String Attractors",
        author: "Samantha Fu, Gautam Prabhu, Abhinav Nigam, Vipul Bhat",
        pdfURI: "assets/pdfs/String_Attractors.pdf",
        text: stringAttractors,
      },
      {
        title: "Black Box Building Blocks",
        pdfURI: "assets/pdfs/Egan.pdf",
        text: blackBox,
      },
      {
        title: "Slavery in the Tempest",
        pdfURI: "assets/pdfs/Tempest.pdf",
        text: tempest,
      },
    ],
  },
  {
    type: "Fiction",
    titles: [
      {
        title: "2 PM on a Sunday Morning",
        pdfURI: "assets/pdfs/2pm.pdf",
        text: sundayAfternoon,
      },
      {
        title: "The Art of Falling",
        pdfURI: "assets/pdfs/Falling.pdf",
        text: falling,
      },
      {
        title: "The Ringing in My Head",
        pdfURI: "assets/pdfs/Ringing.pdf",
        text: ringing,
      },
    ],
  },
  {
    type: "Poetry",
    titles: [
      {
        title: "Romeo in the 21st Century",
        pdfURI: "assets/pdfs/romeo.pdf",
        text: romeo,
      },
      {
        title: "Aubade",
        pdfURI: "assets/pdfs/Aubade.pdf",
        text: aubade,
      },
      {
        title: "Tactical Retreat",
        pdfURI: "assets/pdfs/Tactical_Retreat.pdf",
        text: tactical,
      },
    ],
  },
];

const clearElement = (element = notebook) => {
  element.innerHTML = "";
  return element;
};

const renderMenu = () => {
  clearElement();

  // Add title to notebook
  const title = elem("h1");
  title.textContent = "Writing";
  title.setAttribute("selection-color", "purple");
  notebook.append(title);

  for (let i = 0; i < model.length; i++) {
    const current = model[i];

    // Create section
    const section = elem("section");
    section.className = "section";

    // Add Triangle Icon
    const img = elem("img");
    img.setAttribute("src", "assets/images/pointsideways.png");
    img.setAttribute("selection-color", "purple");
    img.className = "triangle";
    section.append(img);

    // Add header for section
    const type = elem("h2");
    type.textContent = current.type;
    type.setAttribute("selection-color", "purple");
    section.append(type);

    // Add list of titles
    const titles = elem("ul");
    titles.className = "title";
    for (let j = 0; j < current.titles.length; j++) {
      const item = elem("li");
      item.textContent = current.titles[j].title;
      item.setAttribute("selection-color", "purple");
      item.setAttribute("highlight", "purple");

      // Add event listener
      item.onclick = () => {
        renderWork(current.titles[j]);
      };

      titles.append(item);
    }
    section.append(titles);

    // Append section to notebook
    notebook.append(section);
  }
};

/** @param {{title: string, pdf: string, text: string}} work */
const renderWork = (work) => {
  clearElement();

  const workSection = elem("section");
  workSection.className = "work";

  const exit = elem("div");
  exit.className = "exit";
  exit.textContent = "x";
  exit.setAttribute("selection-color", "purple");
  exit.onclick = () => {
    renderMenu();
  };
  workSection.append(exit);

  const header = elem("div");
  header.className = "header";

  const title = elem("h1");
  title.textContent = work.title;
  title.setAttribute("selection-color", "purple");
  header.append(title);

  const link = elem("a");
  link.setAttribute("href", work.pdfURI);
  link.setAttribute("target", "_blank");
  const icon = elem("img");
  link.className = "link";
  icon.className = "pdf";
  icon.setAttribute("src", "assets/images/pdf.png");
  icon.setAttribute("selection-color", "purple");
  link.append(icon);
  header.append(link);
  workSection.append(header);

  if (work.author) {
    const authors = elem("h3");
    authors.textContent = "by " + work.author;
    authors.setAttribute("selection-color", "purple");
    workSection.append(authors);
  } else {
    const authors = elem("h3");
    authors.textContent = "by Samantha Fu";
    authors.setAttribute("selection-color", "purple");
    workSection.append(authors);
  }

  const text = elem("div");
  text.className = "text";
  text.style.whiteSpace = "pre-wrap";
  text.setAttribute("selection-color", "purple");
  text.textContent = work.text;
  workSection.append(text);

  notebook.append(workSection);

  if (text.clientHeight > workSection.clientHeight) {
    const topIcon = elem("img");
    topIcon.className = "top-icon";
    topIcon.setAttribute("src", "assets/images/top.png");
    topIcon.setAttribute("selection-color", "purple");
    topIcon.onclick = () => {
      workSection.scrollTop = 0;
    };
    workSection.append(topIcon);
  }
};

renderMenu();
