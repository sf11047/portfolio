const sundayAfternoon = 
`   Her lipstick is smudged slightly, right at the corner of her mouth where she’s been chewing on a pencil. The bite marks on her pencil are stained that same silky red velvet color that looks so good on her. The rest of her face is bare, a constellation of freckles framed by a pair of thin gold rims. Her hair is pulled into two loose braids that end right above her bare collarbones. She’s wearing a dark choker to complete the look, and I follow the line of her neck up. Her mouth is twisting itself into a frown now, as she reads something on her laptop screen that doesn’t make sense. I think she’s beautiful.
    Beautifully distracting. I look away, at once self conscious. We’re sitting together at my favorite table in the back of the library, hidden by the towering skyline of shelves. There’s books and papers scattered everywhere on the table, physical reminders that I should be working on my homework right now. I pick up my pencil-- I hadn’t even noticed I’d dropped it-- and glance at the calculus textbook in front of me. I’m still on the first page of chapter two, and the bright blue heading is mocking in its exuberance. I try to focus. I try to pretend like Sophie’s not there in front of me and think about math-related things. Let f be a function… The text is tiny and cramped on the page, boring and uninspiring, a sea of black that my eyes get lost in. Before I know it, I’ve reached the end of the paragraph and have no idea what I just read. Functions? Domains? Vectors? Let f be a function... I’m just starting to reread the paragraph when Sophie shifts.
    It’s a welcome distraction. She leans forward, rests her chin on a perfectly manicured hand as a lock of hair frees itself from her braid and falls into her face. Sophie puffs her cheeks and blows at it, but the small breath of air does nothing to dissuade the piece of hair. It blows back a little and then swings right into her face again, a thin strand getting stuck on her lipstick. The tips of her eyelashes tangle with the strands of hair. It cuts a sharp line down her face, asymmetric and imperfect, like a piece of contemporary art. I could frame her face now and stick it in a museum, and it would fit right in. I want to reach out and trace that dark line across her face, touch skin on skin. I want to brush that piece of hair and tuck it behind her ear, show off that pretty face of hers and press my lips to her cheek.
    That thought occurs to me and I rip my eyes away, immediately guilty. Sophie is oblivious but I am not, so I direct my eyes back to my calculus textbook, turning the page so hard that it rips. The noise attracts Sophie’s attention, so I make a big fuss about the state of my poor, poor textbook. I rented it for eleven dollars off of Amazon, so I don’t really care about it, but I trace my fingers over the tear in the paper anyways. It cuts into a diagram of a triangle illustrating some theorem I don’t really understand. I copy it down into my notes. My grip on the pencil is tense and stiff, the letters dark and uneven. I barely make it a few words into the theorem before it’s too much pressure and the tip of my pencil snaps clean off. I swear.
    “Lyssa?” Sophie says. “Are you okay?”
    I nod. Sophie finally tucks that one piece of hair back into her braid and I don’t know how to feel about that. She’s leaning towards me on both arms and I instinctively lean forwards. It’s not a large table. I can feel her breath as she asks me, “Are you sure?”
    I can’t look at her without her looking back at me now, so I find refuge in the dull but familiar lines of my textbook. The blue words have no sympathy.

`

const falling = 
`   Sophie hovers in front of the entrance to the room that’s meant to be her home for the next year— her home, from which she can hear the heavy pulse of loud pop music being played. It’s enough to make her want to leave already, but Sophie’s never been a quitter. She squares her shoulders and lifts her chin, ready to rain hell on her future roommate and make sure they know this behavior absolutely won’t be tolerated in the future, Sophie likes her quiet thank you very much and—
    The girl is very much not what Sophie expects. She’s wearing what looks to be pajamas at three pm in the afternoon, to start, and she’s swinging her hips in time to the music as she’s putting up posters on the wall. The Angry Birds pajama pants are slung low on her hips, the thin fabric barely clinging to her skin. Her dark hair is pulled up into one of those impossibly high cheerleader ponytails and she’s whipping it around as she sings along to the lyrics. It’s everything Sophie would disapprove of, but this girl throws herself around with such abandon that it almost looks fun.
    “Um,” Sophie says, all the words she knows flying out the top of her head.
    The girl startles and bangs into a desk.
    “Oh god,” Sophie says. “Are you okay?”
    She drops her suitcase and rushes into the room. The girl is bent double, arm clutching at her side and Sophie’s worried she’s already somehow managed to maim her roommate when she realizes this crazy girl is laughing, of all things.
    “Oh my god,” the girl says as she straightens up, “I’m so dumb.”
    Then she seems to notice Sophie.
    “Oh, are you Sophie? I’m so sorry, Britta stopped by and then I lost track of time and I’m such a mess right now, wait a sec—”
    The girl hurries over to her phone and almost drops it in her haste to turn off the music. She fumbles with it for a few seconds before turning around and holding out a hand. She smiles. It’s a brilliant smile, Sophie thinks, slightly crooked and it crinkles her eyes, but it’s brilliant.
    “Hi! I’m Alyssa, your new roommate, but you can call me Lyssa,” the girl— Lyssa— says to her. “I’m so sorry about that by the way, I didn’t expect you to be here so early. I’ve been here for a few days for some diversity discovery pre-orientation thing, so I kinda forgot other people exist.”
    It’s sort of a ridiculous explanation and old Sophie would’ve hated it probably, but new Sophie is in college for the first time and doing great new things. New Sophie finds it quite charming. She grabs Lyssa’s outstretched hand and starts to shake it, but Lyssa pulls her into a hug.
    “We’re roommates now,” Lyssa says with a laugh. “We’ll be up close and personal. Might as well start now.”
    Sophie, oddly, finds herself not minding. She’d been worried about making friends at college. She’d been worried about the academics, getting into law school, the shared bathrooms, hair in the shower drain, extracurriculars, but here was one thing less to worry about. Sophie could come to like this Lyssa person.
    “Okay,” Sophie tells her, “then can you turn your music down?”

    Sophie’s not in the apartment when Lyssa lets herself in for the last time. Lyssa had asked this one thing of Sophie, unwilling to pack their past into cardboard boxes with Sophie sitting there on the counter like nothing had changed. Lyssa knows that they’d agreed to be friends— Lyssa wants them to be friends, more than anything— but this apartment had been their safe space together, and having Sophie there would have been too much, too soon. Lyssa doesn’t even know if she can do this alone.
    Both their names are on the lease, but Lyssa had been the one to move out. She can’t stand the thought of it, trapped in that apartment they’d called home, haunted every day by the memories that wouldn’t go away— Sophie’s favorite mug on the counter and never the cabinet, the swear jar Sophie had started just for her, the squishy armchair they used to curl up in together. There had been too much of herself left there, so Lyssa had pieced together the rest and chose to leave it all.
    Lyssa steps into the apartment now, and pauses in the doorway. The lights are off and the curtains closed but the afternoon sun that manages to filter through already illuminates the small differences in the apartment. Lyssa’s clutter of hair ties and bobby pins have been swept up from the counter. There are no more jackets laying on the floor and the books are all organized by color on the shelf above the sofa. There’s a brown box sitting on the kitchen island, clearly labeled with Lyssa’s name. There’s something so final about the sight of that cardboard box with her name on it, like Sophie’s already moved Lyssa out of her life. Lyssa goes into the bedroom. It’s much of the same. Everything is boxed neatly already, half the closet barren, the bathroom empty, the bed stripped of their sheets.
    Lyssa grabs the boxes and hauls them to join the one in the living room. She doesn’t bother to go through them. She trusts Sophie will have everything organized already and will have made sure nothing is left behind. Once all the boxes are outside of the apartment, Lyssa turns around and gives the apartment one last look. The small one bedroom apartment had been cramped, leaky, drafty in the winters but it had been theirs. It looks empty now, the dark casting long shadows in every nook and cranny. It already feels unfamiliar.

    Sophie doesn’t expect to like Lyssa as much as she does. By all rights, they should be polar opposites. If Sophie is quiet like the drizzling of rain, then Lyssa is a whirlwind of energy and it’s all Sophie can do not to be swept away sometimes. Lyssa is loud in all the ways Sophie isn’t, sociable in all the ways Sophie isn’t, fun in all the ways Sophie isn’t. Sometimes she wonders why Lyssa sticks with someone like her, someone plain and ordinary, quiet and hesitant.
    Like now, Lyssa’s sitting at the foot of Sophie’s bed, squinting into a small compact mirror doing her make up. Sophie’s on her bed, trying to get some last minute reading done before they head out for the night, because Sophie is nothing if not dedicated, but the words are already starting swim off the page. Lyssa shifts herself on the floor and leans forward. Her hair falls around her face as she purses her lips to put on lipstick. It’s a dark berry red, and Sophie can’t help but notice how well the color looks on her, with that choker and velvet dress. They’ll be all over Lyssa at the party.
    Sophie, on the other hand, is wearing a short black shift dress, nothing too fancy or revealing, the perfect amount of in between. Her hair is pulled into a simple tight ponytail at the top of her head and her makeup is a minimalist and neutral. To top it all off, she’s reading textbooks in bed while Lyssa’s still dolling herself up. There’s something symbolic in this, Sophie thinks.
    “I don’t understand how you get ready so fast,” Lyssa says, and moves on to powder.
    “That’s because I’m good at makeup,” Sophie shoots back. And like now, when Lyssa doesn’t take offense the way most people do when Sophie says things like this. When Lyssa only laughs at her pointed comments and pouts and replies, “It’s a wasted skill on you.”
    Sophie waves her off, “I’m just too lazy to do it all the time.”	
    “Bullshit,” Lyssa says. “You’ve never been lazy in your life.” 
    “Aren’t your friends waiting for you?”
    “What do you mean?”
    “I mean,” Sophie fumbles, searching for the right words. “I mean, they invited you right? Aren’t you going with them?”
    She doesn’t ask, why are you still waiting for me.
    Lyssa just tilts her head and scoffs at Sophie. 
    “Dumbass,” Lyssa says. “I invited you. Why would I want to go with them?”
    “I don’t know,” Sophie says.
    “Exactly,” Lyssa says, pointing an eyeshadow brush at Sophie. “I’m going with you.”
    She says it without even looking up from the small mirror, like it’s a done deal.

    Lyssa refuses to cry. Sophie won’t look her in the eyes, and Lyssa’s tired of trying to meet Sophie halfway. Sophie’s sitting on the edge of their couch, back straight, legs and arms crossed. Her head is turned down, her bangs falling into her face and covering her eyes. Lyssa is standing in front of her, a foot away, feet planted apart, shoulders up, hands balled into fists. She can feel her nails digging into the tender flesh of her palms and knows there’ll be crescent shaped marks left there. No one moves. They stay like that, a tableau of silence, neither of them knowing what to say now.
    They say the worst of the pain comes after. After the shock and the grief, when your mind’s had a few days to process the fact and reality comes crashing down, but right now all Lyssa feels is numb inside.
    “Okay,” Lyssa says, and barely recognizes her own voice.
    Sophie still won’t look at her.
    “That’s it then?” Lyssa says. “We’re done here?”
    “We’re done,” Sophie’s voice is deathly quiet but her tone final, and if her voice wobbles a bit on that last word, neither of them mentions it.
    “Okay,” Lyssa repeats, because it’s all she can do right now, and they lapse into silence again. They’re at an impasse, because no one tells you what to do after the moment when everything’s gone to shit and you still have to keep going.
    Sophie makes the next move. 
    “I can— I can leave,” she says and pushes herself up. She’s halfway across the living room before Lyssa finally registers it.
    “Wait, Soph—” Lyssa starts to say, “Sophie. You don’t have to go I can—”
    “It’s fine.”
    “But—”
    “Lyssa,” Sophie says without room for argument. “I want to go.”
    Lyssa has nothing to say to this, so she watches as Sophie grabs her keys and coat from next to the door. She watches as Sophie opens the door and leaves, watches as the door closes on her, and then she just’s just watching the door. There’s a scratch in the wood right above the handle, and the paint on the doorknob is chipping, but it had always been home. 
    Some part of Lyssa had become Lyssa and Sophie, two halves of a whole, two sides of the same coin, buy one get one for free. She doesn’t know where to even start untangling the strings of their lives, and she hadn’t thought she would ever need to until Sophie made that first cut and it had all started to unravel.
    Sophie made breakfast that morning. That was the first sign of something wrong, because Sophie never woke before ten if she could help it. And then Sophie had cornered her after breakfast and started talking, and Lyssa had just known.
    Lyssa had wanted it to be a clean break. Like ripping off a Band-Aid. She didn’t want to be like those couples on TV, who fought and screamed and cried and wailed. She’d listened to Sophie rationalize everything in that lawyer way of hers. Lyssa had thought, she’d be so good at it, and then something in her hadn’t known whether or not to be proud and this, she thinks, was when the reality of it all had finally hit her. Lyssa had listened to Sophie go on and on, and then she’d nodded and that had been that.
    And now Sophie is gone and Lyssa is sitting on what used to be their sofa— their sofa, and where does the word their become her— and looking around at everything that they’ve built together, she realizes she needs to leave too.

    Sophie’s sitting at her desk in their room studying for her calculus midterm that’s only a day away when Lyssa’s phone begins to ring. It’s still that obnoxious Justin Bieber song, the one that Lyssa had changed her ringtone to just to annoy Sophie. Lyssa’s gone to the bathroom, so Sophie only sighs and pushes her notes away. She needs a break anyways.
    Lyssa’s phone is laying in the middle of the mess on her bed. There’s a few textbooks, some lost pencils, a T-shirt. Lyssa had been in the middle of cleaning her stuff out, but she’d been doing a better job of getting distracted. Sophie looks at the mess fondly and picks up the phone. The caller ID says it’s Lyssa’s mom, and Sophie doesn’t even think twice about answering.
    “Hi, Mrs. Chen,” Sophie says into the phone. She tucks it between her right ear and shoulders and then starts picking up all the papers on Lyssa’s bed. It looks like they’re her calculus notes.
    “Oh, hi Sophie,” Mrs. Chen says. “Lyssa not there again?”
    “She’s just in the bathroom this time,” Sophie says with a small laugh. 
    “Oh good,” Mrs. Chen says, as Sophie flips through Lyssa’s notes. They’re dated from last year, and Sophie knows for a fact that Lyssa is taking differential equations currently.
    “Yeah, she’s fine,” Sophie says. “Thank you again for the dumplings, Mrs. Chen. They were amazing.”
    “Oh it’s no problem! You’re too skinny, Sophie.”
    “I’m really not,” Sophie says, and she takes Lyssa’s notes over to her desk. She leaves them there for later and then goes back to the mess on Lyssa’s bed. She throws the T-shirt into their hamper— Lyssa had been wearing it out for her run last night— and starts to count the pencils.
    “Nonsense,” Mrs. Chen says per usual. “Will you just let Lyssa know that I’m sending a package up for her soon?”
    “Of course, Mrs. Chen,” Sophie says, and places the eight pencils onto Lyssa’s desk. “I’ll tell her to call you back on Saturday, too.”
    “Thank you so much, dear. And if you need anything, just let me know, okay?”
    “I’m good for now, but thanks,” Sophie tells her.
    “Okay then,” Mrs. Chen says. “Bye, Sophie.”
    “Bye, Mrs. Chen,” Sophie says, right as the door opens and lyssa walks in.
    “Was that my mom?” Lyssa asks her. Lyssa looks at the newly-clean bed for a second and then flops down onto it.
    Sophie picks up Lyssa’s notes, and tries to find the part about series. 
    “Yeah,” Sophie says. “She wants you to know she’s sending another package.”
    “Again?”
    “Yeah,” Sophie shrugs. “She loves you.”
    “Dude,” Lyssa says, “that means they’ll be something for you too. My mom loves you. It’s like you’re family already.”
    Sophie doesn’t say anything. Something inside her twists, and she pushes it down and looks at Lyssa’s brightly color coded notes instead. They’re impeccably neat, easy to understand, with small notes and tips in the margins. Sophie reads, “pull out the negative one when plugging the interval back in,” and doesn’t think about how easily she’s become a part of Lyssa’s life, and how easily Lyssa’s become a part of hers.

    Lyssa doesn’t even know what they’re fighting about anymore. It had started, as most fights do, with something stupid. Sophie had been sitting on the sofa, with her feet on the coffee table again, when Lyssa had gotten home from her yoga class. Lyssa had been deciding whether or not it would be worth it to tell Sophie to move her feet when Sophie said, “Did you pick up the milk?” 
    Lyssa had winced, and then gotten angry at herself for wincing. 
    “Sorry,” she’d said, somewhat testily, and guessed that Sophie had picked up the tone in her voice.
    “Ly-ssa,” Sophie had said, stressing both syllables. She hadn’t even looked up from her laptop. “It’s been three days.”
    “So-phie,” Lyssa had said back. “Why don’t you get it then? I don’t even like milk.”
    “You pass by the store everyday, though.”
    “So?”
    “So.”
    And Sophie’s tone had been sharp, rude, too much of a challenge for Lyssa to ignore and it had all fallen to shit after that. Lyssa had sneered at Sophie, and then the yelling had started and now they’re here on opposite sides of the room, and Lyssa’s so, so tired of it all. It feels like all they can do nowadays is snipe at each other, and Lyssa doesn’t know when or how this happened, only that she wants it to stop. 

    Lyssa’s is holding Sophie’s hand. Sophie doesn’t know what to make of it. Lyssa is easy with her affection in a way that Sophie is not. Lyssa’s never afraid to hug her friends, or drape an arm around them but with Sophie she does things like this a lot, kisses her on the cheek and grabs her hand, lays her head on Sophie’s shoulder and leans on her. And Sophie lets her because this is Lyssa— Lyssa, who pushes her but never too far; Lyssa, who can see through all of Sophie’s walls; Lyssa, who feels like home. Lyssa, who Sophie supposes she is a bit more than gone for.
    Sophie’s made her peace with this knowledge, but then Lyssa goes and does things like this and Sophie doesn’t know what to do. 
    There’s really no reason for Lyssa to still be holding Sophie’s hand. They’re at the Spring Fair. Lyssa had had no problem with shoving her way through the crowds, but Sophie, as tiny as she is, had kept getting lost and Lyssa had finally lost her patience. She’d grabbed Sophie by the hand and pulled her through the crowds to the ice cream truck, but she hadn’t let go and they’re here now, waiting for their ice cream, holding sweaty hands. And Sophie’s not complaining, not really, but they’ve been dancing around each other for so long, and there is really on so much a girl can take before going crazy, so she pulls at her hand.
    Lyssa responds by holding tighter. She’s staring at her phone pointedly.
    “Lyssa,” Sophie says, and she still doesn’t look up.
    “Lyssa,” Sophie repeats, and yanks at their hands.
    Lyssa still doesn’t let go, but she does look up and say, “What?”
    Sophie shakes their joined hands. Lyssa gives Sophie a weird look, so Sophie sighs.
    “This,” she says, pointing at their hands. “What’s this?”
    “I’m holding your hand,” Lyssa says, and she looks puzzled.
    “But why?”
    “Why not?”
    Lyssa’s answer is distracted, casual, and everything Sophie hadn’t wanted to hear. It pisses her off, so she yanks their hands apart.
    “Lyssa,” Sophie starts. “We’re not even da—”
    Lyssa cuts her off by pressing a small kiss on Sophie’s lips. She smiles at Sophie, something bright and brilliant and beautiful, and as shocked as Sophie is right now, she can’t help but smile back.
    “That’s why,” Lyssa says, and it’s as good an answer as Sophie’s ever heard.

    Sophie is gone again. This is the fifth time in as many days, and Lyssa’s starting to get annoyed. She used to come home from her daily runs to find Sophie lounging on the sofa, or busy in the kitchen with dinner. She’d usually have some music going, or sometimes an audiobook, and it had looked so right to have Sophie there waiting for her, like some scene out of a fairytale or storybook. She’d say, “I’m home,” and Sophie wouldn’t be so domestic as to say, “Welcome back, honey,” but she’d yell at Lyssa to help with the food or to clean her shoes off, and that had been enough.
    Today Lyssa had gotten home, and Sophie was nowhere to be found. She hadn’t even left a note or anything. She’d taken to disappearing in her own Sophie way, without notice or warning. She’d been doing this often, lately, and Lyssa doesn’t know what it means. 
    Today, Lyssa doesn’t even bother searching their bedroom or the bathroom. She just sets her things down by the door knowing that Sophie will complain it’s not the right place for her bags, and starts prepping for dinner. She’s in the middle of washing the carrots when she hears the door open, but Lyssa doesn’t say anything. Lyssa waits for Sophie to get settled and find her in the kitchen. Lyssa’s done too much searching, and it should be Sophie’s turn.
    Sure enough, Sophie wanders into the kitchen. She grabs an apple from the bowl in the kitchen and then pushes herself to sit on one of the counter tops. Lyssa starts cutting the carrots. Sophie watches her. Lyssa ignores her.
    “Can you move your stuff from the door?” Sophie finally asks.
    Lyssa shrugs. She finishes cutting the carrots and moves onto the celery.
    “Please?” Sophie says.
    “Where have you been?” Lyssa asks her instead.
    Sophie pauses. She does that thing where she plays with her hair, twirls and twists it around her fingers, and Lyssa just knows she’s trying to come up with something to say.
    “You know I’m studying for my LSATs,” Sophie says.
    “Yeah, but you could let me know,” Lyssa points out. She doesn’t mention that Sophie’s said that for the past three times, too, or that it’s too early to be studying this obsessively.
    “I’m sorry I don’t tell you where I go every second of the day,” Sophie snaps and Lyssa wants to say, but you used to tell me about your day and I just want that back. But Sophie is fidgeting in her spot on the counter, playing with some of the forks Lyssa had set out and swinging her legs. Lyssa can tell that she’s restless, itching for a fight, so she lets it drop.

    They’re laying in Sophie’s bed together, the covers drawn over both of their bodies. Sophie is curled up on her side, hands on the laptop as they decide what to watch. Lyssa is laying behind her, arms wrapped around her waist, their legs hopelessly tangled. She can feel the warmth of Lyssa’s skin on hers everywhere they touch. She’s hyper aware of the way Lyssa breaths on the sensitive skin at the back of her neck, the way she murmurs into Sophie’s hair and shifts her body closer.
    “What do you want to watch?” Sophie asks, and tries not to shiver.
    “I don’t care,” Lyssa says, and Sophie could swear she’s closer now, if that were possible. “You choose.”
    “Okay,” Sophie says. “We’re watching The Flash.”
    Lyssa groans into her hair. “I take it back. The Flash is terrible.”
    “It is not!” Sophie says. “You told me to choose.”
    “And I take it back. You’re terrible.”
    Sophie turns around to face Lyssa. Her face is barely an inch away from Sophie’s, their breaths mingling.
    “That’s bullying,” Sophie tells her, crossly.
    “It’s not bullying if it’s the truth.” 
    “That’s it,” Sophie says. “This is done. We’re over.”
    She makes to get up, but Lyssa tightens her grip around Sophie’s body, and no matter how hard she tries, Sophie can’t get away.
    “St— stop that,” Sophie says between breaths of laughter. “Oh my god, you’re terrible.”
    “No that’s you,” Lyssa tries to say, but can’t help laughing. “Ow! You kicked me.”
    “You—You’re tickling me!”
    “Too bad.”
    “I hate you.”
    “I hate you too, babe.”
    And Sophie’s sort of dying because Lyssa’s squeezing so hard and she can’t stop laughing but this is more than enough, to have the two of them here like this.

    It’s somewhere near midnight. Lyssa is lying in bed and watching the way the moonlight plays on the sheets. Sophie’s next to her, curled up in her multitude of blankets. She thinks Sophie’s asleep, when she hears, “I can’t believe we’re almost seniors.”
    Lyssa turns to look at Sophie. Sophie is facing away from her, turned toward the window by their bed. She looks so small there, swathed in all her pillows and curled into a little ball. Lyssa puts an arm around her.
    “I know,” she says.
    “It’s like, where did the time go?”
    “I don’t know.”
    “I have to study for the LSATs!” Sophie says this with a slight hysterical tone to her voice.
    “Sophie,” Lyssa says. She runs her hand through Sophie’s hair and presses a kiss to the back of her head. “You’re so brilliant. You’ll be fine. You’ll go to Harvard ,graduate top of your class, and become a kickass lawyer.”
    “I don’t even like Harvard,” Sophie mumbles into her sheets. 
    “Then it’ll be Stanford, or Yale.”
    “You don’t know that.”
    “No,” Lyssa says. “But I know you.”
    Sophie says something so quietly that Lyssa can’t hear. She moves herself closer to Sophie presses another kiss to her shoulder.
    “What was that?”
    “Where will you be?” Sophie asks.
    “What?”
    “I’ll be at law school. Where will you be?”
    Lyssa is quiet. It sounds like it should be a simple question but it raises a whole host of problems. Lyssa knows she should have a better idea. She’s been doing engineering classes because she loves math and thinks it would carry over but it’s not anything she’d expected. 
    “I don’t know,” Lyssa says.
    “What do you mean?”
    “I don’t know what I want to do,” Lyssa says, and buries her face in Sophie’s hair. “It’s all a mess.”
    “But what about this?” Sophie asks, and her voice only wobbles a little bit. Oh, Lyssa thinks. 
    “Sophie,” Lyssa says, and tries to think what to say. “It’ll be okay. I know that much..”
    “Okay,” Sophie says, and they fall silent.

    Sophie is playing on her phone, taking a short break from studying for midterms when she gets the notification on her phone. It’s an email from housing, so she pauses her game and taps on the notification. 
    Dear Blue Jays, the email reads, with your Housing and Dining application due in a week, we’d like to remind you to submit your preferences through the housing portal by March 30!
    Sophie almost curses. She’d nearly forgotten about the deadline, and looks over to Lyssa. She’s hunched over her desk and scribbling at a piece of paper rather aggressively. Sophie had been meaning to ask Lyssa about their rooming situation, but she’d gotten nervous too many times and chickened out. Their relationship isn’t necessarily new, but there’s still times where Sophie doesn’t know where they stand. The email is a pointed reminder though, so Sophie decides now or never.
    “Lyss?” 
    Lyssa makes a noise that’s a cross between a hum and a groan, so Sophie assumes she’s heard.
    “What are we doing for rooming next year?”
    “What do you mean?” Lyssa doesn’t even look up from her math.
    “I mean, what are we doing?”
    “Dummy,” Lyssa says. “We’re rooming together.”
    “We are?”
    Lyssa pauses. “I mean, only if you want to—”
    “No I definitely do—”
    “Well then,” Lyssa says, like this should be common knowledge, “we’re rooming together. I already filled out my form. I was thinking we could look for an apartment off campus?”
    “Oh,” Sophie says, and looks at her textbook. “Yeah, that sounds amazing.”
    “Great,” Lyssa says, and goes back to her studying. She looks at her notes, and then groans outright and throws her head onto the desk. “I hate Professor Brown. Why does he hate us?”
    “I don’t know,” Sophie says. Lyssa throws her hands into the air and then picks herself up to start again. Sophie turns back to her textbook, but she can’t keep a stupid smile off her face, or concentrate enough.

    Living with Sophie is the same, but also different. Lyssa doesn’t know how to explain it, only that there’s something final and responsible and adult-like about moving in together like this. This is their choice, not some random happenstance determined by the college roommate selection algorithm and they’ve chosen each other this time. It means more to Lyssa than she can say, that out of all the chances and people in this world, they’ve managed to find each other.
    It’s overwhelming, but in a good way.
    They’re cleaning up the pseudo-housewarming party they’d thrown now, and finding all the bottles and cans amidst all the brown boxes is proving to be surprisingly difficult. Lyssa doesn’t even mind it too much though, because she’s still marveling over the idea of Sophie and Lyssa’s place. An apartment to call their own, someplace that is wholly theirs and theirs alone.
    After an hour they call it quits and crash on the couch. It’s late, but neither of them wants to move to the bed. It would involve getting up and untangling their limbs. Lyssa is leaning on one side of the couch, and Sophie is leaning on her, practically in her lap. They’re both quiet, but they’re both content in this moment to sit there together. Lyssa closes her eyes and Sophie shifts so that her head is leaning on Lyssa’s shoulder. Lyssa rests her chin on Sophie’s head and presses a small kiss on top. She lets herself rest like that. Lyssa’s almost fallen asleep there when Sophie moves herself. She pushes herself up from her spot against Lyssa. Lyssa hums, the small unspoken question lingering in the air. Sophie turns to look at Lyssa in the eye. Sophie’s hair is a mess around her face, all tangled and frizzy in the worst way possible. The bags under her eyes are more prominent than ever and she looks like she’s about to crash. She’s smiling at Lyssa, and she looks beautiful.
    “Lyss,” Sophie says. “Alyssa, Lyssa, Lyss.”
    “Yes?” Lyssa smiles at Sophie.
    “I love you,” Sophie says, and then rests her head against Lyssa’s shoulder again. 
    “I love you,” she repeats like she’s trying the words out. “I love you.”
    Lyssa lays her head on Sophie’s again. Sophie’s hair smells like strawberries, the shampoo she uses, and Lyssa could lose herself forever in the sweet scent of it. There’s a stray tear in her eye, as Lyssa whispers, “I love you too.” She closes her eyes. “Sophie Favreaux. Love you.”
    And there’s something in the both of them that feels like air, something buoyant and light and breathless and incredible. Lyssa feels like she could face the world like this, with Sophie at her side. The world would be no match for the two of them.

`

const ringing = 
`[5 missed calls]
    The doorbell rang as another man in an ill-fitting business suit stepped into the store to buy a wedding ring. The sound of his shiny, patent leather shoes tapped a slow rhythm on the tiled floor, hesitant. He gazed wide-eyed at the polished glass cases lined with arrays of precious gems he’d probably spend the rest of his life paying off. I asked him what his girlfriend liked and he stared at me with those same stupid eyes. After a moment he told me she liked shiny things, so I directed him towards the diamonds. I watched this man waffle about deciding between cuts of diamond that would ultimately never matter. One of them would probably cheat, and it would get out, and the divorce would be messy and complicated. They’d have kids, maybe, who’d get caught up in the mess. It was surprising how many men came in looking like that, nervous and unsure of themselves, like they were trapping themselves with a band of metal. They took love for granted, lured by the dream of the nuclear family they never had—suburban delusions.
    Lina called me after I got back to my apartment, while I was making dinner. It was the fourth time today, according to my phone, and I figured I had better answer her before she came kicking down the door to my apartment. Lina was like that: pushy, annoyingly so at times, but only because she cared. I picked up her call, put the phone on speaker, placed it two feet away on top of the microwave, and returned to peeling my potatoes for the curry.
    “Julie,” Lina screeched into the phone. I almost dropped a potato. “Why didn’t you answer me?
    “I was at work.”
    “You’re always at work,” she said. Her voice, already naturally high, sounded like a barbie doll’s coming through the tinny speakers of my phone. “Come out with us tonight! Have some fun!”
    “I don’t have fun.”
    “Bullshit,” she said immediately. “Is this about your mom again?”
    “No.” 
    “Why don’t you just block her?”
    I swept the potato skins into the trashcan and reached for the knife. “I told you,” I said, “she’s talking to my brother again. Ryan gave her”—the knife sliced down with a loud thud against the cutting board—”my number. If I block her, she’ll ask him for my address and then—fuck!”
    I’d lost focus and the knife had slipped, grazing the edge of my knuckles. The skin looked red and raw.
    “What happened?”
    “Nothing,” I said, sucking on the injury.
    “Julie, are you—” 
    “I’m great, okay? I’ll go out with you guys.”
    “Wait really?”
    “Don’t sound so surprised.”
    Lina laughed, like I knew she would, her mind already moving on to later tonight. “Okay, see you later then! It’s been forever since you’ve gone out with us.”
    I could hear the exclamation points. Better yet, I could practically see them floating in the air, punctuating every sentence. Her enthusiasm was almost infectious. “Sure,” I said.
    The karaoke bar Lina picked was a small, hole-the-wall place crammed in between a liquor store and Asian market. I would’ve walked right past it if I hadn’t seen Lina’s favorite checkered coat, standing in the middle of a small group of people. The neon red sign above the door read Family Music Studio in English and Mandarin that I couldn’t read underneath. Kind of a stupid name for a karaoke bar, I told Lina as a greeting. She told me to play nice and then shooed everyone inside. Apparently, I had been the last to arrive.
    I pulled her aside as everyone else went inside. “You didn’t say there was going to be this many people,” I grumbled.
    "Hush,” she said, holding a finger up to my lips. Almost everyone was inside now, except for the guy who had been holding the door. He looked like he was waiting for us. “Make some friends. That’s an order.”
    Then she giggled and leaned in close, which really should have been my hint to leave. Instead I watched as she jerked her head in the direction of doorman and whispered, “I think you’ll really like him. Really like him.” As if we were in the second grade again.
    I stared at her. Doorman looked like every Asian mom’s ideal son-in-law. Hair swept artfully to the side, wearing a sweater vest and scarf to a karaoke bar, he would’ve been more at home on the front page of a college brochure, casually reading Kant under the golden red leaves of his favorite oak on campus. Or maybe at Stanford. I will admit that he was attractive in a boy-next-door, will-treat-you-right kind of way, if you were into that. He had a splash of freckles on his face, and the tips of his ears were turning cold. At least he hadn’t gone so far as to wear earmuffs yet. 
    Lina winked at me.
    “Yeah, we’ll fall in love in a whirlwind romance and get married and have four kids and you’ll see us on the cover of housing magazines.”
    “I can’t wait,” she said, unperturbed. She hooked an arm around my side and steered us towards him and the door, winking as we passed him inside. He held the door open for us like a gentleman, of course.
    The room we had booked definitely wasn’t built to fit as many people as we had, but we made do. I sat in back on one of the couches and flipped through the holy bible of songs, looking up a song whenever someone wanted to play it. Lina went once, and then twice, and drank a lot more than that. Boy-next-door-man went once too, and his voice wasn’t even that bad. Lina went a third time and once she finished, she curtsied and pointed towards me. I shook my head at her, but she came towards me with the mic anyways.
    “Julie! I love you!” Lina said, throwing herself on top of me “Did you know that I love you?”
    "You told me that already,” I said, but she wasn’t listening.
    "You know, that bitch doesn’t deserve you or Ryan. Did you know that?” Lina grabbed my face into her hands and pulled me close. “Tell me you know that.”
    “O-kay,” I said, and pried myself free from her grip. “That’s enough for you, I think.”
    “No, Julie,” she kept saying. “Listen to me. Listen.”
    “Nope,” I said and stood up. Lina had been leaning all her weight on me and she flopped down into the now empty seat. The only other open spot was next to my future husband, but it was either talk to drunk Lina or sit there. I walked across the room. He had taken off his sweater, I noticed, and was wearing a dress shirt with the first three buttons undone. He sat leaning back on the couch and watched me squeeze myself onto a sofa that really wasn’t big enough to fit all the people on it. Our legs pressed against each other in the heat of the room, shoulders bumping as people moved around. He turned to face me, close enough that I could feel his breath.
    “So,” he said, “you’re the elusive Julie.”
    “How do you know my name?”
    He nodded towards Lina, who was lying across three different people’s laps. “Lina talks about you all the time, but you never show up. I was beginning to think she’d made you up.”
    “Well, here I am,” I said with a shrug.
    “Here you are,” he agreed. “And I have to say, I’m a little disappointed.”
    “What?”
    “The way Lina talks about you, I thought you’d be… oh, I don’t know. I thought you’d show up all sharp edges, ready to show everyone up.”
    “What the hell? That’s what you think of me?”
    “And instead,” he said, leaning in close. His hair was falling out of place into his eyes, and he was smiling at me. “Instead, you’re here sulking in a corner too chicken-shit to sing.”
    “Fuck you,” I said, and stood up. “Give me the fucking mic.”

[3 missed calls]
    Lina was right. His name was Aiden, and I went home that night. He liked leaving marks. Lina screeched when she saw them and was insufferable for a week. She kept saying I told you so until I blocked her number and then wouldn’t stop bothering me until I unblocked her.
    Aiden texted me the day after. He asked if we could meet at a coffeeshop or somewhere and I said yes and this time he came home with me. He kept surprising me, and I liked that about him. He was addicted to protein shakes and kicked people in his sleep. He hated cheese but wasn’t lactose intolerant and watched too much anime. He wore sweater vests and leather jackets and did have a pair of earmuffs because he had bad circulation but had forgotten them that night. He liked cooking, but only for other people. 
    He spent a lot of time over at my apartment in the next few weeks, cooking and filling my apartment with the scent of turmeric and star anise. He’d busy himself with dicing vegetables while I leaned on the countertop next to him feeling useless in my own kitchen. When I tried to help, he’d point a stalk of celery at me, or maybe broccoli, or on one occasion a singular green bean, and tell me to leave it alone.
    “You’d just burn the kitchen down,” he’d say, not even looking up from the sauce pan. “I don’t know what you did before me.”
    “Excuse me,” I told him, and bumped him out of the way with my hip. “I’m taking over tonight.”
    Dinner was slightly charred that night. Aiden wouldn’t stop making fun of me the entire time we spent watching Inception afterwards, so I made him stop instead, pressing my lips to his. That night we laid in bed, whispering in the dark. Aiden was easy to talk to in a way that most people weren’t. He didn’t take things too seriously and kept up with most of my sarcasm. Sometimes it felt like I had too much to say to him.
    “I want to meet your friends,” he told me as we faced each other in the dark. His hand was resting on the side of my check, rough and calloused.
    “You have.”
    “Who?”
    “Lina.”
    He laughed. “Lina can’t be your only friend.”
    I pushed myself up onto an elbow, the weight of his hand falling away from my face. 
    “You’d be wrong about that,” I told him.
    Aiden turned away from me and onto his back. “Fine then,” he said. “Meet my friends.”
    “What?”
    “You heard me,” he said. “We’re having dinner in a week. I want you to be there.”
    I rolled over onto my back, staring at the lines of moonlight shining through the blinds onto the ceiling. I was silent.
    “Unless you don’t want to,” Aiden added, after a moment. He sounded so unsure of himself, in a way that I had never heard before.
    I shook my head, but he couldn’t see it in the dark. “No, it’s fine. I’ll go.”

    Aiden’s friends were the type of people who planned dinners in advance using when2meets, who updated each other on their lives and celebrated every little success, who were free with their affections and hugged each other as a greeting. They were the sort of people who held regular potlucks in their apartments, like the one we were going to.
    The inside of the apartment was worn and cozy. There were shoes scattered in front of the door and coats piled on a chair in the living room. The place was clean but not too clean, scattered with obvious signs that the place was lived in—envelopes stacked on a table, dishes piled in the kitchen sink, photographs hanging on the walls. People I didn’t recognize sat lounging on the floor or the sofas, talking quietly amongst themselves. Someone was playing Mario Kart on the TV and losing badly. The low light cast a warm glow over the entire scene, at once familial yet unfamiliar.
    A woman in her mid-20s jumped up as soon as she saw us enter. She was tall, with thick dark hair that was bleached at the bottom. The shape of her eyes, the way she held herself—it was familiar. I looked towards Aiden, and then back at her. He nodded. 
    “Yeah, that’s my sister,” he said as she walked up to us.
    “Tess,” she said, holding out a hand to me. “Aiden’s my baby brother.”
    “Don’t listen to anything she says,” Aiden told me very seriously. “She’s a demon.”
    Tess laughed. “Oh, don’t be like that,” she said and put an arm around my shoulders, steering me towards the dining room as Aiden followed us. “It’s been so long since Aiden’s brought someone to dinner. I can’t wait to tell embarrassing stories. Did you know he tried to eat a stapler once?”
    I sat sandwiched between Tess and Aiden during dinner. Everyone seemed to have a good time, joking around and passing dishes of food. There was yelling, but only because the room was loud. Tess and Aiden made fun of each other all night, but it was never mean spirited. It was like one of those American sit coms where people just sat around a dinner table and had a good time. Too good a time. I lost track of the times Aiden laughed that night. 
    It was obvious that everyone there had been friends for years, and that I was the stranger. There were too many stupid jokes I didn’t get, too many references I couldn’t understand. I focused on my food instead, answering questions about my aspiring career goals (not be unemployed) and my future plans (undefined, thanks). 
    “So,” Tess said after we finished eating. She leaned towards me conspiratorially, with a grin on her face. “How did you guys meet?”
    I shrugged. “Karaoke.”
    “Yeah, and Julie was being a hermit in the corner.” Aiden said, laughing. He tried to sling an arm around my shoulders, but I shrugged it off.
    “No, I wasn’t,” I said.
    “Yes, you were,” Aiden said as if it was the most obvious thing in the world. The rest of what he was going to say was cut off by the sound of Chinese pop playing from my phone. I had forgotten to silence my phone.
    “You like Jay Chou?” Aiden asked incredulously. “I thought you’d be into MCR or some shit.”
    “Don’t be mean,” Tess said. “Do you need to pick it up?” 
    I wasn’t listening to either of them. I recognized the number on my phone. Ryan had given it to me after mama had called him the first time. It was not the first time she had called me today, but I hadn’t answered then and I wasn’t going now.
    “Julie?” Aiden said, and I looked up from the phone. 
    “What?”
    “Tess asked if you need to pick it up.”
    “Yeah,” I said, shaking my head. “I mean, no, I don’t. What are we talking about?”
    Aiden looked at me, but Tess cut in. 
    “Music,” she said. “Do you listen to Rainie Yang?”
    The rest of the night passed uneventfully. Tess had a lot to say about Chinese pop, which wasn’t something I knew a lot about but was interesting to hear. Aiden stayed by my side all night which wasn’t necessary at all. Everyone was so nice, almost cloyingly so, and did their best to include me in conversation. As we left, Tess even gave me a hug and told me to come again some time. “With or without Aiden,” she said with a wink.
    We walked home that night, enjoying the cool night air before the weather turned truly cold. Aiden was unusually quiet along the way, seemingly absorbed in his thoughts. When we reached the door to my apartment, he grabbed my hand before we could go outside.
    “Julie,” he said and paused, considering his words. “Did you have fun tonight?”
    I shrugged. ‘Yeah, of course.”
    “Okay,” he said, nodding. “My friends liked you a lot. So did Tess.”
    “Does it matter?”
    “Of course it matters, Julie.” 
    “They barely even know me. I don’t care about them. I like you.”
    He sighed and pulled me closer. “Tess is my sister.”
    I nodded slowly.
    “Forget it,” he said irritably, and let go of my hand. “This is impossible.”
    “What?”
    “You don’t ever talk to me, Julie.” He sounded actually angry, which was new to me. “You don’t ever talk to me.”
    That annoyed me. I talked to him more than I talked to most people, even Lina. “Yes, I do. I tell you about my day and the people at work and my favorite commercials.”
    “Sure, but what about important things? Are you even okay? Who called you at dinner? You’ve been out of it ever since. Don’t pretend you don’t know what I’m talking about either.”
    “No one. My mom. Don’t worry about it.”
    He threw his hands into the air. “See that’s exactly what I’m talking about.”
    “Fine! It was my mom. I just—I don’t want to talk about her. It’s personal.”
    I didn’t realize I was shaking until Aiden placed his hands on my shoulder and steadied me. “Calm down,” he said. “I’m sorry for pushing you.”
    I nodded. “It’s fine.”
    We were silent. Aiden decided he didn’t want to stay over that night, so I let him leave and went up to my apartment alone. The expanse of my room somehow felt so looming, like the emptiness would swallow me whole. It was stupid. It had been barely a month before when I’d been perfectly fine sleeping alone. I stole a blanket off my bed and wrapped it around myself instead.
    The night air was dry and cold as I stepped out onto my balcony. I sat on the edge, dangling my legs over the street as I watched occasional stragglers walk by. I liked sitting there, floating above the business of the city, half in and half removed. It was past midnight, but the city never really went to sleep. It slowed down if anything, trapped in limbo for the weird hours between night and morning when the only people who were awake were drunk or overthinking. There was something about nighttime that just begged for introspection, that recognized your vulnerability and capitalized on it, dragging all the things you didn’t want to thinking about to the front of your mind. It was kind of rude, I though. I didn’t want to think about those things for a reason.

[4 missed calls]
    Lina barged into my apartment the next day, as she was wont to do. I was sitting on my couch watching TV when she let herself in rather dramatically, slamming my door in the process. She was carrying a bottle of wine and waving it around.
    “We need to talk,” she said, and then went to rummage for glasses in my kitchen.
    “Okay,” I said, still watching TV. “Thanks for coming over, I guess.”
    She poured us glasses of Sangria and then proceeded to tell me about her job interview and the girl she had met last night and the new apple pie recipe she’d tried last night. I nodded along. Lina kept pouring me glasses, subtly enough that I didn’t notice how much I had been drinking until she finally got to her point.
    “How are you and Aiden doing?” she asked me.
    “Oh, you know,” I said, waving my hand around vaguely. “He’s great. I like him.”
    “I do know, actually, because he’s my friend too Julie.”
    “What is that supposed to mean?”
    “It means,” Lina said slowly, “that he tells me things too.”
    “Like what?” I said and finished my glass in one go.
    “And I know you, Julie. Get your shit together. It’s not your fault your mom’s a dick but you don’t have to hurt everyone else too. Just tell Aiden.”
    “It’s my choice,” I said. “You don’t get to decide that.” 
    “Just think about it,” she said, getting up and putting on her jacket. “You can keep the wine.”
    “I thought you were my friend first.”
    “It doesn’t work like that.”
    “Fuck you,” I said, unable to stop myself. “Of course it doesn’t. It never does. The people who should care never do.”
    “Oh, Julie,” Lina had said with such pity in her voice that I couldn’t stand it. I pointed at the door. She was doing the exact same thing everyone had ever done, what the one person I should’ve been able to trust had done.

[0 missed calls]
    The last time I saw mama was the day I moved out. Of course, that didn’t stop her from yelling at me the entire time I was packing my things. She was mercurial, and the smallest thing could set her off. Ryan had been there that day helping move out. She’d been criticizing the clothes he was wearing, one of her favorite topics. They were too feminine, too gaudy—people would get the wrong idea about us, she had said and I had snapped at her because Ryan would never defend himself. She had started screaming, something about ungrateful children who would never amount to anything, and I had started screaming too. Ryan kept carrying my boxes to the car as we faced off against each other. There had never been any defining moment—mama had never beat me or anything like that the way all the movies made abuse look like, but it had been every microaggression, every hurtful comment and betrayal of trust. By then, I had lived in that house too long, left every small hurt stewing for so long that nothing made sense anymore. I’d cut my losses and left as soon as I could, cutting off all contact. I had thought that leaving would magically fix everything, but I had been wrong.
    The next time mama called, I picked up.
    “So,” mama said, “finally decided to talk to your poor, old mother?”
    “Let’s get this straight,” I started. I looked down at the piece of paper in my hand, at all the points I had carefully constructed in preparation for this conversation. “You’re not poor, and you’re not even that old. I don’t owe you anything. Stop calling me.”
    “I just want to talk,” she said in an overly sweet voice that made me sick. “Ryan’s been talking to me.”
    “I don’t care! Ryan can do whatever he wants, but I don’t want to talk to you.”
    Her tone immediately changed.
    “Of course you don’t. You’ve never cared about family have you, you un—”
    “This isn’t what family is!” I yelled into the phone. I thought about Lina, so earnest in her support even when drunk and even while I was being rude. I thought of Aiden, who had only wanted to help, and his sister, and friends. “It took me so, so long to figure that out and it’s your fault. It’s all your fault, it always has been.”
    “You don’t get to blame me, you—”
    “You don’t get to make excuses. You don’t get a second chance. You’re the reason why I have trust issues, you’re the reason why I’m so fucked up. Leave me alone.”
    I hung up. I knew I was shaking.
    I’d forgotten how easily she set me off, how all that hurt had never really settled in my skin. It threatened to bleed now, the wound ripped open again and I could only see red.
    Before I even realized it, I had called Aiden. I stared at the phone in my hand, at the contact flashing on the screen. I could hear his voice through the phone faintly. He was repeating my name, I think. Julie. Julie. Are you okay. I don’t think I said anything and he said I’m coming over and I said okay and hung up. I sat on my couch. I was angry, angry at myself for wanting him to come over, angry for wanting to talk to him. It boiled inside me, the need for someone else, and I felt uneasy in my own skin—like a puzzle piece that didn’t quite fit.
    I don’t know how much time passed. I knew that one minute I was fuming on my couch, and the next minute I was letting Aiden into my apartment and pushing him against a wall. He put his hands on me, but all he did was push me away.
    “Julie,” he said with his hands on my shoulders. “What’s wrong.
    He was looking at me. I didn’t want to look at him.
    “Nothing,” I said.
    “I don’t believe you,” he said, and kept looking at me, like there was something wrong with me. “Tell me what’s wrong.”
    “Nothing’s wrong.”
    “You’re lying to me,” he said, and waited. I didn’t say anything.
    “Let me guess,” he said under his breath, “you don’t want to talk about.”
    I think it was the tone he used that really set me off—a little mocking, a little too pointed, and all too familiar. I looked down at my phone. One missed call, it read. Grabbing his wrists, I pulled myself free and took a step forward into his space.
    “Why do you even care? What am I—some distraction from your, your picture perfect life and picture perfect family and picture perfect friends?”
    “I like you,” he yelled right back at me. This was, at least, familiar ground. “I let you meet Tess.”
    “So what,” I said. I was in his face now, inches apart. I could have kissed him if I wanted. “It’s not even like you’re my boyfriend.”
    I saw the hurt on Aiden’s face immediately.
    “You know what,” he said, his voice colder than I had ever heard it. “I don’t care anymore. You’re right, Julie. I’m not your boyfriend.”
    And then he turned his back on me, reaching for the door and opening it. He was still wearing his coast and scarf, I saw. He hadn’t even taken them off. Aiden looked back at me one last time, his figure silhouetted in the door frame. “Do what you want, Julie, but don’t call,” he said. The door closed, a moment passed. Then I ran.
    He had too much of a head start. He was too tall, his legs longer than mine. He had probably been running, eager to get out of my apartment building, eager to leave me behind. I burst through the doors of my apartment building and into the street, almost knocking over some poor woman carrying her groceries. 
    It was a Saturday afternoon. The streets were crowded with people rushing here and there, absorbed in their thoughts. A couple crossed the street ahead of me. A family stood arguing off to the side. No one on this street knew me except for a familiar figure wearing a blue scarf, already a block ahead. I stood there in the middle of the street, watching the flow of their people as they went about their lives, unaware of the world around them. Later Lina would ask me why I didn’t call him to apologize and I would tell her about love and men in ill-fitting business suits, but right then I watched Aiden’s figure fade into a blue spot in the distance. I didn’t follow him.
`



