# Portfolio

Personal website for Samantha Fu [Live here](https://sf11047.gitlab.io/portfolio).

The design of the website is based on my love of bullet journaling, writing, art, Zebra Mildliner
Highlighters, and Pilot G2 Pens. I wanted something quirky and unique that combined all my interests
in one page and allowed me to display my professional and personal work.

I decided to build the website from scratch using pure HTML, CSS, and JS because this was my first web dev
experience and I wanted to get a handle on the basics before delving into other web frameworks. The
website is still currently in progress - you can find a list of todos below. The Library link in the nav
bar opens a stand in page for now, but will link to a seperate library/book reccomendations website
that I am currently working on.

## TODO

Ordered by priority.

- ~~Finalize First Design Iteration~~
- ~~Structure~~
  - ~~Page Skeletons~~
    - ~~index~~
    - ~~about~~
    - ~~books~~
    - ~~projects~~
    - ~~writing~~
- ~~Writing Page~~
  - ~~Formating of menu~~
  - ~~Formatting of individual samples~~
  - ~~Add Author~~
  - ~~Back Button~~
  - ~~Back to Top Button~~
- About Me Page
  - Drawing
  - Text styling
- Project Page
  - ~~Add git links~~
  - ~~Add live links~~
- Styling
  - Full size styling
  - Mobile responsiveness
  - Add custom font
  - ~~Link styling~~
  - ~~Color~~
- nav-links
  - ~~basic highlighter image~~
  - ~~Animated highlighter on hover~~
- Art
  - ~~G2 Pen for left side of notebook~~
  - ~~Notebook rings~~
  - Animations
- Favicon
- Library
  - Create Library website (a seperate project)
